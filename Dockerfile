FROM ubuntu:22.04

RUN apt-get update && apt-get install -y \
    git \
    python3 \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*


RUN pip3 install Flask gunicorn pytest pytest-cov

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

WORKDIR /app

COPY . /app
RUN chmod +x deploy.sh
expose 9060 9061

ENTRYPOINT ["bash", "deploy.sh"]
